const startTimestamp = Date.now();
const startTime = new Date();
const scrollPosition = {};
const popupsVisibility = [];

if (
  window.location.hostname !== 'nemecek.sk' &&
  window.location.hostname !== 'localhost'
) {
  addCustomWebStyle();
  addClickEventsForLinks();
  setInterval(addClickEventsForLinks, 1000);
  addScrollEvent();
  addPopupEvents();
}
showPage();

// --- Functions definitions ---

function addScrollEvent() {
  window.addEventListener('scroll', () => {
    scrollPosition[Date.now()] = window.scrollY;
  });
}

function showPage() {
  htmlElement = document.getElementsByTagName('html')[0];
  htmlElement.style.visibility = 'initial';
}

function addClickEventsForLinks() {
  console.log('DP Experiment - Trackujem linky...');
  let anchors = document.getElementsByTagName('a');
  for (let i = 0; i < anchors.length; i++) {
    anchors[i].addEventListener('click', trackClick);
  }
  disableClickEventForSpecificSites();
}

function parseSelectedURL(url) {
  if (url.includes('//')) {
    url = url.split('//')[1];
  }

  if (url.includes('www.')) {
    url = url.split('www.')[1];
  }

  url = url.endsWith('/')
    ? url.slice(0, -1)
    : url;

  return url;
}

function createFinalJson(event, currentNode) {
  const urlParams = new URLSearchParams(window.location.search);
  const sessionId = urlParams.get('sessionId');

  let finalObject = {
    sessionId,
    startTime,
    endTime: new Date(),
    scrollPosition,
    popupsVisibility,
    selectedElement: parseSelectedURL(currentNode.href),
    solutionTime: Math.round(Date.now() - startTimestamp),
    baseURI: currentNode.baseURI,
    className: currentNode.className,
    id: currentNode.id,
    innerHTML: currentNode.innerHTML,
    innerText: currentNode.innerText,
    nodeName: currentNode.nodeName,
    outerHTML: currentNode.outerHTML,
    outerText: currentNode.outerText,
    clientX: event.clientX,
    clientY: event.clientY,
    timeStamp: event.timeStamp,
    type: event.type,
  };

  return JSON.stringify(finalObject);
}

async function sendJson(json) {
  const urlParams = new URLSearchParams(window.location.search);
  const participant = urlParams.get('participantUsername');
  const taskId = urlParams.get('taskId');

  return new Promise(function (resolve, reject) {
    let xhr = new XMLHttpRequest();
    xhr.open(
      'PUT',
      `https://nemecek.sk/participants/${participant}/tasks/${taskId}`
    );
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = function () {
      if (this.status >= 200 && this.status < 300) {
        console.log(`Event odoslany na BE`);
        resolve(xhr.response);
      } else {
        reject({
          status: this.status,
          statusText: xhr.statusText,
        });
      }
    };
    xhr.onerror = function () {
      reject({
        status: this.status,
        statusText: xhr.statusText,
      });
    };
    xhr.send(json);
  });
}

function redirectToNextTask() {
  const urlParams = new URLSearchParams(window.location.search);
  const participant = urlParams.get('participantUsername');
  const taskId = urlParams.get('taskId');
  window.location = `https://nemecek.sk/participants/${participant}/tasks/${taskId}/results`;
}

async function trackClick(event) {
  event.preventDefault();
  const finalJson = createFinalJson(event, this);
  console.log(`Odosielam event - ${this.href}`);
  const result = await sendJson(finalJson);
  redirectToNextTask();
}

/* Track pop-ups */

function addPopupEvents() {

  // ---> zastavmekorupciu.sk
  if (
    window.location.hostname === 'zastavmekorupciu.sk' ||
    window.location.hostname === 'www.zastavmekorupciu.sk'
  ) {

    // Newsletter element
    let newsletterElement = document.querySelector("#popup-newsletter");
    let newsletterObserver = new MutationObserver(mutationRecors => {

      if (popupsVisibility[0]) {
        popupsVisibility[0].hidden = new Date();
        console.log(`Pop-up newsletter zatvoreny v case: ${popupsVisibility[0].hidden}`);
      } else {
        popupsVisibility[0] = {
          element: '#popup-newsletter',
          visible: new Date()
        };
        console.log(`Pop-up newsletter otvoreny v case: ${popupsVisibility[0].visible}`);
      }
    });

    newsletterObserver.observe(newsletterElement, {
      attributes: true,
    });

    // Cookies bar

    let cookiesElement = document.querySelector(".cookie-message");
    popupsVisibility[1] = {
      element: '.cookie-message',
      visible: new Date()
    };
    console.log(`Cookie bar otvoreny v case: ${popupsVisibility[1].visible}`);

    let cookiesObserver = new MutationObserver(mutationRecors => {
      if (popupsVisibility[1]) {
        popupsVisibility[1].hidden = new Date();
        console.log(`Cookie bar zatvoreny v case: ${popupsVisibility[1].hidden}`);
      }
    });

    cookiesObserver.observe(cookiesElement, {
      attributes: true,
    });
  }

  // ---> 4ka.sk
  if (
    window.location.hostname === '4ka.sk' ||
    window.location.hostname === 'www.4ka.sk'
  ) {

    // Chat pop-up

    let chatObserver = new MutationObserver(mutationRecors => {

      if (popupsVisibility[0]) {
        popupsVisibility[0].hidden = new Date();
        console.log(`Chat zatvoreny v case: ${popupsVisibility[0].hidden}`);
      }
    });

    let bodyElement = document.querySelector("body");
    let bodyObserver = new MutationObserver((mutationRecors) => {
      let chatElement = document.querySelector("#botmedia-chat-content");
      if (!chatElement) return;

      popupsVisibility[0] = {
        element: '#botmedia-chat-content',
        visible: new Date()
      };
      console.log(`Chat otvoreny v case: ${popupsVisibility[0].visible}`);
      bodyObserver.disconnect();

      chatObserver.observe(chatElement, {
        attributes: true
      });
    });

    bodyObserver.observe(bodyElement, {
      childList: true,
      subtree: true,
    });

    // Cookies bar

    let cookiesElement = document.querySelector("#cookie-bar");
    popupsVisibility[1] = {
      element: '#cookie-bar',
      visible: new Date()
    };
    console.log(`Cookie bar otvoreny v case: ${popupsVisibility[1].visible}`);

    let cookiesObserver = new MutationObserver(mutationRecors => {
      if (popupsVisibility[1]) {
        popupsVisibility[1].hidden = new Date();
        console.log(`Cookie bar zatvoreny v case: ${popupsVisibility[1].hidden}`);
      }
    });

    cookiesObserver.observe(cookiesElement, {
      attributes: true
    });
  }

  // ---> gigastore.sk
  if (
    window.location.hostname === 'gigastore.sk' ||
    window.location.hostname === 'www.gigastore.sk'
  ) {

    // Wheel pop-up element
    let wheelElement = document.querySelector('#koloo-plg');
    let wheelObserver = new MutationObserver(mutationRecors => {
      if (popupsVisibility[0]) {
        if (mutationRecors[0].target.style.display === 'none') {
          popupsVisibility[0].hidden = new Date();
          console.log(`Pop-up koleso zatvorene v case: ${popupsVisibility[0].hidden}`);
        }
      } else {
        popupsVisibility[0] = {
          element: '#koloo-plg',
          visible: new Date()
        };
        console.log(`Pop-up koleso otvorene v case: ${popupsVisibility[0].visible}`);
      }
    });

    wheelObserver.observe(wheelElement, {
      attributes: true,
      attributeFilter: ["style"]
    });
  }

  // ---> bugy.sk

  if (
    window.location.hostname === 'bugy.sk' ||
    window.location.hostname === 'www.bugy.sk'
  ) {

    // Pop-up element

    let popupObserver = new MutationObserver(mutationRecors => {
      if (popupsVisibility[0]) {
        popupsVisibility[0].hidden = new Date();
        console.log(`Pop-up zatvoreny v case: ${popupsVisibility[0].hidden}`);
      }
    });

    let bodyElement = document.querySelector("body");
    let bodyObserver = new MutationObserver((mutationRecors) => {
      let popupElement = document.querySelector('#popupContainer');
      if (!popupElement) return;

      popupsVisibility[0] = {
        element: '#popupContainer',
        visible: new Date()
      };
      console.log(`Popup otvoreny v case: ${popupsVisibility[0].visible}`);
      bodyObserver.disconnect();

      popupObserver.observe(popupElement, {
        attributes: true
      });
    });

    bodyObserver.observe(bodyElement, {
      childList: true,
      subtree: true,
    });
  }

  // ---> trisisky.sk

  if (
    window.location.hostname === 'trisisky.sk' ||
    window.location.hostname === 'www.trisisky.sk'
  ) {
    let cookieObserver = new MutationObserver(mutationRecors => {
      if (popupsVisibility[0]) {
        popupsVisibility[0].hidden = new Date();
        console.log(`Cookie bar zatvoreny v case: ${popupsVisibility[0].hidden}`);
      }
    });

    let bodyElement = document.querySelector("body");
    let bodyObserver = new MutationObserver((mutationRecors) => {
      let cookieElement = document.querySelector('#elementor-popup-modal-2321');
      console.log(document.querySelector('#elementor-popup-modal-2321'));
      if (!cookieElement) return;

      popupsVisibility[0] = {
        element: '#elementor-popup-modal-2321',
        visible: new Date()
      };
      console.log(`Cookie bar otvoreny v case: ${popupsVisibility[0].visible}`);
      bodyObserver.disconnect();

      cookieObserver.observe(cookieElement, {
        attributes: true
      });
    });

    bodyObserver.observe(bodyElement, {
      childList: true,
      subtree: true,
    });
  }

  // ---> utuloknz.sk
  if (
    window.location.hostname === 'utuloknz.sk' ||
    window.location.hostname === 'www.utuloknz.sk'
  ) {
    let popupElement = document.querySelector('#popmake-5283');
    let popupObserver = new MutationObserver(mutationRecors => {
      if (popupsVisibility[0]) {
        if (mutationRecors[0].target.style.display === 'none') {
          popupsVisibility[0].hidden = new Date();
          console.log(`Pop-up zatvoreny v case: ${popupsVisibility[0].hidden}`);
        }
      } else {
        popupsVisibility[0] = {
          element: '#popmake-5283',
          visible: new Date()
        };
        console.log(`Pop-up otvoreny v case: ${popupsVisibility[0].visible}`);
      }
    });

    popupObserver.observe(popupElement, {
      attributes: true,
      attributeFilter: ['style']
    });
  }

  // ---> okey.sk

  if (
    window.location.hostname === 'okay.sk' ||
    window.location.hostname === 'www.okay.sk'
  ) {
    let popupElement = document.querySelector('#popup-box');
    let popupObserver = new MutationObserver(mutationRecors => {
      if (popupsVisibility[0]) {
        if (mutationRecors[0].target.style.display === 'none') {
          popupsVisibility[0].hidden = new Date();
          console.log(`Pop-up zatvoreny v case: ${popupsVisibility[0].hidden}`);
        }
      } else {
        popupsVisibility[0] = {
          element: '#popup-box',
          visible: new Date()
        };
        console.log(`Pop-up otvoreny v case: ${popupsVisibility[0].visible}`);
      }
    });

    popupObserver.observe(popupElement, {
      attributes: true,
      attributeFilter: ['style']
    });
  }

  // ---> zahradkar

  if (
    window.location.hostname === 'www.zahradkar.pluska.sk' ||
    window.location.hostname === 'zahradkar.pluska.sk'
  ) {
    let cookieObserver = new MutationObserver(mutationRecors => {

      if (popupsVisibility[0]) {
        popupsVisibility[0].hidden = new Date();
        console.log(`Cookie zatvoreny v case: ${popupsVisibility[0].hidden}`);
      }
    });

    let bodyElement = document.querySelector("body");
    let bodyObserver = new MutationObserver((mutationRecors) => {
      let cookieElement = document.querySelector('.gdpr_consent_popup');
      console.log(cookieElement);
      if (!cookieElement) return;

      popupsVisibility[0] = {
        element: '.gdpr_consent_popup',
        visible: new Date()
      };
      console.log(`Cookie otvoreny v case: ${popupsVisibility[0].visible}`);
      bodyObserver.disconnect();

      cookieObserver.observe(cookieElement, {
        attributes: true
      });
    });

    bodyObserver.observe(bodyElement, {
      childList: true,
      subtree: true,
    });
  }


  // ---> recepty.aktuality.sk
  if (
    window.location.hostname === 'www.recepty.aktuality.sk' ||
    window.location.hostname === 'recepty.aktuality.sk'
  ) {

    let bodyElement = document.querySelector('body');
    popupsVisibility[0] = {
      element: '.fc-consent-root',
      visible: new Date()
    };
    console.log(`Cookie bar otvoreny v case: ${popupsVisibility[0].visible}`);

    let cookiesObserver = new MutationObserver(mutationRecors => {
      let cookiesElement = document.querySelector('.fc-consent-root');
      if (cookiesElement != null) {
        return;
      }

      if (popupsVisibility[0]) {
        popupsVisibility[0].hidden = new Date();
        console.log(`Cookie bar zatvoreny v case: ${popupsVisibility[0].hidden}`);
        cookiesObserver.disconnect();
      }
    });

    cookiesObserver.observe(bodyElement, {
      attributes: true,
      childList: true,
      subtree: true,
    });
  }

  // ---> www.vw.sk
  if (
    window.location.hostname === 'vw.sk' ||
    window.location.hostname === 'www.vw.sk'
  ) {

    // Cookies

    let cookiesObserver = new MutationObserver(mutationRecors => {
      if (popupsVisibility[0]) {
        if (mutationRecors[0].target.style.display === 'none') {
          popupsVisibility[0].hidden = new Date();
          console.log(`Pop-up zatvoreny v case: ${popupsVisibility[0].hidden}`);
        }
      }
    });

    let bodyElement = document.querySelector("body");
    let bodyObserver = new MutationObserver((mutationRecors) => {
      let cookiesElement = document.querySelector('#onetrust-banner-sdk');
      if (!cookiesElement) return;

      popupsVisibility[0] = {
        element: '#onetrust-banner-sdk',
        visible: new Date()
      };
      console.log(`Cookies bar otvoreny v case: ${popupsVisibility[0].visible}`);
      bodyObserver.disconnect();

      cookiesObserver.observe(cookiesElement, {
        attributes: true
      });
    });

    bodyObserver.observe(bodyElement, {
      childList: true,
      subtree: true,
    });
  }
}


/* -- Web settings -- */

function disableClickEventForSpecificSites() {

  if (
    window.location.hostname === 'zastavmekorupciu.sk' ||
    window.location.hostname === 'www.zastavmekorupciu.sk'
  ) {
    let element = document.querySelector("a.popup-newsletter__close");
    if (element) element.removeEventListener('click', trackClick);
  }

  if (
    window.location.hostname === '4ka.sk' ||
    window.location.hostname === 'www.4ka.sk'
  ) {
    let element = document.querySelector("a.cb-enable");
    if (element) element.removeEventListener('click', trackClick);
  }

  if (
    window.location.hostname === 'bugy.sk' ||
    window.location.hostname === 'www.bugy.sk'
  ) {
    let element = document.querySelector("a#cookie-bar-button");
    if (element) element.removeEventListener('click', trackClick);
  }

  if (
    window.location.hostname === 'trisisky.sk' ||
    window.location.hostname === 'www.trisisky.sk'
  ) {
    let element = document.querySelector("#elementor-popup-modal-2321 div.dialog-message.dialog-lightbox-message a.elementor-button-link");
    if (element) element.removeEventListener('click', trackClick);
  }

  if (
    window.location.hostname === 'okay.sk' ||
    window.location.hostname === 'www.okay.sk'
  ) {
    let elements = document.querySelectorAll("a#popup-close");
    if (elements) {
      for (const element of elements) {
        element.removeEventListener('click', trackClick);
      }
    }

    element = document.querySelector("a#k-close");
    if (element) element.removeEventListener('click', trackClick);
  }

  if (
    window.location.hostname === 'krups.sk' ||
    window.location.hostname === 'www.krups.sk'
  ) {
    let element = document.querySelector('a[href="#documentation"]');
    if (element) element.removeEventListener('click', trackClick);
  }

  if (
    window.location.hostname === 'recepty.aktuality.sk' ||
    window.location.hostname === 'www.recepty.aktuality.sk'
  ) {
    let element = document.querySelector('a#confirmCookieBar');
    if (element) element.removeEventListener('click', trackClick);
  }

  if (
    window.location.hostname === 'pizzerka.sk' ||
    window.location.hostname === 'www.pizzerka.sk'
  ) {
    let element = document.querySelector('a#scnb-cookie-accept');
    if (element) element.removeEventListener('click', trackClick);
  }

  if (
    window.location.hostname === 'sencor.sk' ||
    window.location.hostname === 'www.sencor.sk'
  ) {
    let element = document.querySelector("a.cookie-law-approval-close");
    if (element) element.removeEventListener('click', trackClick);

    let elements = document.querySelectorAll('a[href="#product-download"]');
    for (const element of elements) {
      element.removeEventListener('click', trackClick);
    }
  }

  if (
    window.location.hostname === 'epenta.sk' ||
    window.location.hostname === 'www.epenta.sk'
  ) {
    let elements = document.querySelectorAll("a.btn.btn-buy");
    for (const element of elements) {
      element.removeAttribute('onclick');
      element.setAttribute('href', '/buy');
    }
  }

  if (
    window.location.hostname === 'vub.sk' ||
    window.location.hostname === 'www.vub.sk'
  ) {
    let element = document.querySelector('a[href="#zalozit-ucet"]');
    if (element) element.removeEventListener('click', trackClick);
  }
}

/* Web style */

function addCustomWebStyle() {
  if (
    window.location.hostname === 'www.apple.com' ||
    window.location.hostname === 'apple.com'
  ) {
    document.querySelector("section.section.section-hero p.typography-intro.intro-cta").style.display = 'none';
    setTimeout(() => {
      document.querySelector("div.compare-section.section-buy div.compare-column.compare-column-one").insertAdjacentHTML("afterbegin", '<a class="device-link more" href="/sk/iphone-12/" aria-label="iPad&nbsp;12&nbsp;mini – zistiť viac" data-analytics-title="iphone 12&nbsp;mini - learn more">Zistiť viac</a>');
      document.querySelector("div.compare-section.section-buy div.compare-column.compare-column-two").insertAdjacentHTML("afterbegin", '<a class="device-link more" href="/sk/iphone-12/" aria-label="iPhone&nbsp;12 – zistiť viac" data-analytics-title="iphone 12 - learn more">Zistiť viac</a>');
      document.querySelector("div.compare-section.section-buy div.compare-column.compare-column-three").insertAdjacentHTML("afterbegin", '<a class="device-link more" href="/sk/iphone-12-pro/" aria-label="iPhone&nbsp;12&nbsp;Pro&nbsp;Max – zistiť viac" data-analytics-title="iphone 12 pro max - learn more">Zistiť viac</a>');
    }, 1000)
  }

  if (
    window.location.hostname === 'www.zahradkar.pluska.sk' ||
    window.location.hostname === 'zahradkar.pluska.sk'
  ) {
    let bodyElement = document.querySelector("body");
    let bodyObserver = new MutationObserver((mutationRecors) => {
      let ad = document.querySelector(".branding_top");
      if (ad) ad.style.display = 'none';
      ad = document.querySelector(".branding_l_r");
      if (ad) ad.style.display = 'none';
    });

    bodyObserver.observe(bodyElement, {
      childList: true,
      subtree: true,
    });
  }

  if (
    window.location.hostname === 'vub.sk' ||
    window.location.hostname === 'www.vub.sk'
  ) {
    let element = document.querySelector(".cookies_policy");
    if (element) element.style.display = 'none';
  }

  if (
    window.location.hostname === 'www.zabokreky.sk' ||
    window.location.hostname === 'zabokreky.sk'
  ) {
    let cookie = document.querySelector("#cookie-alert");
    if (cookie) cookie.style.display = 'none';
  }

  if (
    window.location.hostname === 'sad-po.sk' ||
    window.location.hostname === 'www.sad-po.sk'
  ) {
    let element = document.querySelector('a[href="index.php?show=13"]');
    if (element) element.style.display = 'none';
  }
}