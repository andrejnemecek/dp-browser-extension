Doplnok do prehliadača, pre experiment k mojej diplomovej práci.


1. Naklonuj alebo stiahni tento repozitár
2. Stiahni si Google Chrome - https://www.google.com/chrome/
3. Načítaj tento extension do prehliadača:
   ![Otvor rozšírenia](https://gitlab.com/andrejnemecek/dp-browser-extension/-/raw/master/experiment-1.jpg)

4. Otvor celý priečinok stiahnutý z gitu
   ![Načítaj rozšírenie](https://gitlab.com/andrejnemecek/dp-browser-extension/-/raw/master/experiment-2.jpg)

5. Povol tento extension v režime inkognito:
   ![Nastavenia rozšírenia](https://gitlab.com/andrejnemecek/dp-browser-extension/-/raw/master/experiment-3.jpg)
   ![Povol režim inkognito](https://gitlab.com/andrejnemecek/dp-browser-extension/-/raw/master/experiment-4.jpg)

6. Začni experiment v režime inkognito na tejto URL: https://nemecek.sk/experiment/control/participants/new/tasks/0
7. **Po experimente nezabudni deaktivovať extension, lebo ti nepôjde klikať na odkazy :)**
